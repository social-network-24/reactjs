import gql from "graphql-tag";
import {useMutation, useQuery} from "@apollo/client";
import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardMeta,
  Form,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Image,
  Label
} from "semantic-ui-react";
import moment from "moment";
import LikeButton from "../components/LikeButton";
import React, {useContext, useRef, useState} from "react";
import {AuthContext} from "../context/auth";
import DeleteButton from "../components/DeleteButton";
import {useNavigate, useParams} from "react-router-dom";
import MyPopup from "../utils/MyPopup";

function SinglePost() {
  const navigate = useNavigate();
  const {postId} = useParams();
  const {user} = useContext(AuthContext);
  const commentInputRef = useRef(null);
  const [comment, setComment] = useState("");

  const {loading, data} = useQuery(FETCH_POST_QUERY, {
    variables: {
      postId
    }
  });

  const [submitComment] = useMutation(SUBMIT_COMMENT_MUTATION, {
    update() {
      setComment("");
      commentInputRef.current.blur();
    },
    variables: {
      postId,
      body: comment
    }
  });

  function deletePostCallback() {
    navigate("/");
  }

  let postMarkup;
  if (loading) {
    postMarkup = <p>Loading post...</p>;
  } else {
    const {id, body, createdAt, username, comments, likes, likeCount, commentCount} = data.getPost;
    postMarkup = (
      <Grid>
        <GridRow>
          <GridColumn width={2}>
            <Image src="https://react.semantic-ui.com/images/avatar/large/molly.png"
                   size={"small"} float={"right"}
            />
          </GridColumn>
          <GridColumn width={10}>
            <Card fluid>
              <CardContent>
                <CardHeader>{username}</CardHeader>
                <CardMeta>{moment(createdAt).fromNow()}</CardMeta>
                <CardDescription>{body}</CardDescription>
              </CardContent>
              <hr/>
              <CardContent extra>
                <LikeButton user={user} post={{id, likeCount, likes}}/>
                <MyPopup content={"Comment on post"}>
                  <Button as={"div"} labelPosition={"right"} onClick={() => console.log("comment on post...")}>
                    <Button basic color={"blue"}>
                      <Icon name={"comments"}/>
                    </Button>
                    <Label basic color={"blue"} pointing={"left"}>{commentCount}</Label>
                  </Button>
                </MyPopup>

                {user && user.username === username && (
                  <DeleteButton postId={id} callback={deletePostCallback}/>
                )}
              </CardContent>
            </Card>
            {user && (
              <Card fluid>
                <CardContent>
                  <p>Post a comment</p>
                  <Form>
                    <div className={"ui action input fluid"}>
                      <input type={"text"} placeholder={"comment..."} name={"comment"} value={comment}
                             onChange={(event) => setComment(event.target.value)}
                             ref={commentInputRef}
                      />
                      <button type={"submit"} className={"ui button teal"} disabled={comment.trim() === ""}
                              onClick={submitComment}>Submit
                      </button>
                    </div>
                  </Form>
                </CardContent>
              </Card>
            )}
            {
              comments.map((comment) => (
                <Card fluid key={comment.id}>
                  <CardContent>
                    {user && user.username === comment.username && (
                      <DeleteButton postId={id} commentId={comment.id}></DeleteButton>
                    )}
                    <CardHeader>{comment.username}</CardHeader>
                    <CardMeta>{moment(comment.createdAt).fromNow()}</CardMeta>
                    <CardDescription>{comment.body}</CardDescription>
                  </CardContent>
                </Card>
              ))
            }
          </GridColumn>
        </GridRow>
      </Grid>
    );
  }
  return postMarkup;
}

const SUBMIT_COMMENT_MUTATION = gql`
    mutation submitComment($postId: ID!, $body: String!) {
        createComment(postId: $postId, body: $body) {
            id
            comments {
                id body createdAt username
            }
            commentCount
        }
    }
`;

const FETCH_POST_QUERY = gql`
    query getPost($postId: ID!) {
        getPost(postId: $postId){
            id
            body
            createdAt
            username
            likeCount
            likes {
                username
            }
            commentCount
            comments{
                id
                username
                createdAt
                body
            }
        }
    }
`;

export default SinglePost;