import React, {useContext, useState} from "react";
import {Button, Form, FormInput} from "semantic-ui-react";
import gql from "graphql-tag";
import {useMutation} from "@apollo/client";
import {useNavigate} from "react-router-dom";
import {useForm} from "../utils/hooks";
import {AuthContext} from "../context/auth";

function Register() {
  const context = useContext(AuthContext);
  const navigate = useNavigate();
  const [errors, setErrors] = useState({});

  const {onChange, onSubmit, values} = useForm(registerUser, {
    username: "", email: "", password: "", confirmPassword: ""
  });

  const [addUser, {loading}] = useMutation(REGISTER_USER, {
    update
      (_
        , {data: {register: userData}}
      ) {
      context.login(userData);
      navigate("/");
    },
    onError(err) {
      setErrors(err.graphQLErrors[0].extensions.errors);
    },
    variables: values
  });

  function registerUser() {
    addUser();
  }


  return (
    <div className={"form-container"}>
      <Form onSubmit={onSubmit} noValidate={true} className={loading ? "loading" : ""}>
        <h1>Register</h1>
        <FormInput type={"text"}
                   label={"Username"}
                   placeholder={"Username"}
                   name={"username"}
                   value={values.username} onChange={onChange}
                   error={errors.username ? true : false}
        />
        <FormInput type={"email"}
                   label={"Email"}
                   placeholder={"Email"}
                   name={"email"}
                   value={values.email} onChange={onChange}
                   error={errors.email ? true : false}/>
        <FormInput type={"password"}
                   label={"Password"}
                   placeholder={"Password"}
                   name={"password"}
                   value={values.password} onChange={onChange}
                   error={errors.password ? true : false}
        />
        <FormInput type={"password"}
                   label={"Confirm Password"}
                   placeholder={"Confirm Password"}
                   name={"confirmPassword"}
                   value={values.confirmPassword} onChange={onChange}
                   error={errors.confirmPassword ? true : false}
        />
        <Button type={"submit"}
                primary>
          Register
        </Button>
      </Form>
      {Object.keys(errors).length > 0 && (
        <div className={"ui error message"}>
          <ul className={"list"}>
            {
              Object.values(errors).map(value => (
                <li key={value}>{value}</li>
              ))
            }
          </ul>
        </div>
      )
      }
    </div>
  );
}

const REGISTER_USER = gql`
    mutation register (
        $username: String!
        $email: String!
        $password: String!
        $confirmPassword: String!
    ) {
        register(registerInput: {
            username: $username
            email: $email
            password: $password
            confirmPassword: $confirmPassword
        }) {
            id
            email
            username
            createdAt
            token
        }
    }
`;


export default Register;