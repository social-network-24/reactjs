import React, {useContext} from "react";
import {useQuery} from "@apollo/client";
import {Grid, GridColumn, GridRow, TransitionGroup} from "semantic-ui-react";
import PostCard from "../components/PostCard";
import {AuthContext} from "../context/auth";
import PostForm from "../components/PostForm";
import {FETCH_POSTS_QUERY} from "../utils/graphql";


function Home() {
  const {user} = useContext(AuthContext);
  const {
    loading,
    error,
    data
  } = useQuery(FETCH_POSTS_QUERY);
  const posts = data?.getPosts;
  return (
    <Grid columns={3} divided>
      <GridRow className={"page-title"}>
        <h1>Recent Posts</h1>
      </GridRow>
      <GridRow>
        {user && (
          <GridColumn>
            <PostForm/>
          </GridColumn>
        )}
        {
          loading ? (
            <h1>Loading posts...</h1>
          ) : (
            <TransitionGroup>
              {
                posts && posts.map(post => (
                  <GridColumn key={post.id} style={{marginBottom: 20}}>
                    <PostCard post={post}/>
                  </GridColumn>
                ))
              }
            </TransitionGroup>
          )
        }
      </GridRow>
    </Grid>
  );
}

export default Home;