import React, {useContext} from "react";
import {AuthContext} from "../context/auth";
import {Navigate, Route} from "react-router-dom";


function AuthRoute({component: Component, ...rest}) {
  const {user} = useContext(AuthContext);
  return <Route {...rest} render={props => user ? <Navigate replace to={"/"}/> : <Component {...props} />}/>;
}

export default AuthRoute;