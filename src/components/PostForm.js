import {Button, Form, FormField, FormInput} from "semantic-ui-react";
import {useForm} from "../utils/hooks";
import gql from "graphql-tag";
import {useMutation} from "@apollo/client";
import {FETCH_POSTS_QUERY} from "../utils/graphql";

function PostForm() {
  const {onChange, onSubmit, values} = useForm(createPostCallback, {body: ""});
  const [createPost, {error}] = useMutation(CREATE_POST_MUTATION, {
    variables: values,
    update(proxy, result) {
      const data = proxy.readQuery({
        query: FETCH_POSTS_QUERY
      });
      const updatedPosts = [result.data.createPost, ...data.getPosts]; // Combine new post and existing posts
      // data.getPosts = [result.data.createPost, ...data.getPosts];
      proxy.writeQuery({
        query: FETCH_POSTS_QUERY,
        data: {getPosts: updatedPosts}
      });
      console.log(result);
      values.body = "";
    },
    onError(err) {
      console.error(err);
    },
  });

  function createPostCallback() {
    createPost();
  }

  return (
    <>
      <Form onSubmit={onSubmit}>
        <h2>Create a post</h2>
        <FormField>
          <FormInput
            placeholder={"Hi World"}
            name={"body"}
            onChange={onChange}
            value={values.body}
            error={!!error}
          />
          <Button type={"submit"} color={"teal"}>Submit</Button>
        </FormField>
      </Form>
      {error && (
        <div className={"ui error message"} style={{marginBottom: 20}}>
          <ul className={"list"}>
            <li>{error?.graphQLErrors[0]?.message}</li>
          </ul>
        </div>
      )
      }
    </>
  );
}

const CREATE_POST_MUTATION = gql`
    mutation createPost($body: String!) {
        createPost(body: $body){
            id body createdAt username
            likes {
                id username createdAt
            }
            likeCount
            comments {
                id body username createdAt
            }
            commentCount
        }
    }
`;

export default PostForm;